const router = require('express').Router()
const qs = require('qs')

const mongo = require('./../modules/db.js')

let collection


// below code checks if connected to database. If connected execute main function

const dbCheck = setInterval(() => {
  const x = mongo.db()
  const pending = mongo.pending()
  if (!pending && x && x !== undefined && x !== null) {

    console.log('\x1b[32m%s\x1b[0m', "Connected to DATABASES | SEARCH")

    collection = mongo.collection

    clearInterval(dbCheck)
  }
}, 1)

router.get('/:query', (req, res) => {
	const params = qs.parse(req.params.query)
	const qw = decodeURIComponent(params.query)
	const limit = Number(params.limit)

	const query = [
		{
			schoolname: {$regex : qw, $options: 'i'}
		},
		{
			address: {$regex : qw, $options: 'i'}
		},
		{
			pincode: {$regex : qw, $options: 'i'}
		},
		{
			area: {$regex : qw, $options: 'i'}
		},
		{
			landmark: {$regex : qw, $options: 'i'}
		}
	]

	console.log('user searched - ' + qw)

	collection('signzy', 'records').find({$or: query}).limit(limit).toArray()
	.then((result) => {
		res.status(200)
		res.json({result})
	}).
	catch((err) => {
		console.error(err)
	})
})

module.exports = router