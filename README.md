<h2>Project architecture</h2>

"signzy-submission-backend" consist of server application logic written in javascript (nodeJS), as well as dist files for front-end application

"signzy-submission-frontend" consist of source files written in vue.js (javascript framework).<br><br> 

To clone source files for frontend application from Gitlab's remote repo, follow below commands (optional)<br>
<code>git clone https://gitlab.com/chiragiem36/signzy-submission-frontend.git</code>

<code>cd signzy-submission-frontend</code>

<code>sudo npm install -g</code>

Note - "signzy-submission-backend" and "signzy-submission-frontend" should be under same parent folder.

-------------------------------

<h2>Installing backend application</h2>

To clone from Gitlab remote repo -<br>
<code>git clone https://gitlab.com/chiragiem36/signzy-submission-backend.git</code><br><br>

To install libraries (required)<br>
<code>cd signzy-submission-backend</code><br>
<code>sudo npm install -g</code>

To start server - <br>
<code>sudo pm2 start app.js</code>

note - When server starts, it firsts parse data.csv and stores information in remote mongodb server

open browser and visit "http://localhost:8000/dist/index.html"

-----------------------------


<h2>API</h2>

<code>/search/query=:query&limit:limit</code>

request to above path returns an array of results
<h4>Parameters</h4><br>
<b>query</b> - keyword to search<br>
<b>limit</b> - Number of results to return<br><br>
Example - "http://localhost:8000/search/query"