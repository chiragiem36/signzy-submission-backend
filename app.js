const express=require('express')
const app = express()
const bp = require('body-parser')
const fs = require('fs')
const path = require('path')
const port = 8000
const search = require('./routes/search.js') // module that handles all request that starts with '/search'

const mongo = require('./modules/db.js')

let collection


// below code checks if connected to database. If connected execute main function

const dbCheck = setInterval(() => {
  const x = mongo.db()
  const pending = mongo.pending()
  if (!pending && x && x !== undefined && x !== null) {

    console.log('\x1b[32m%s\x1b[0m', "Connected to DATABASES")

    collection = mongo.collection

    clearInterval(dbCheck)
    main()
  }
}, 10)


//main function
function main () {
	const docs = []
	const data = fs.readFileSync(path.resolve('./data.csv')).toString() // read .csv file sync

	// parsing .csv file
	allLines = data.split('\n')

	const fields = allLines.shift().split('|') // parsing first line to get fields
	fields.unshift('_id')
	allLines.shift()

	class Document {
		constructor (lineArr) {
			fields.forEach((field, n) => {
				this[field] = lineArr[n]
			})
		}
	}

	allLines.forEach((line, n) => { // parsing all lines and creating Object out of it
		const lineArr = line.split('|')
		lineArr.unshift(n)
		const doc = new Document(lineArr)
		docs.push(doc)
	})

	collection('signzy', 'records').insertMany(docs)
	.then(() => {
		console.log("All records inserted")
	})
	.catch((err) => {
		console.error("Failed to insert some or all records")
	})

	app.use(bp.json())
	app.use('/search', search)
	app.use('/dist', express.static("dist",{
    'maxAge': '604800'
  }))
}

app.listen(process.env.PORT || port,function(){
	console.log('\x1b[44m%s\x1b[0m', "listening on port " + port)
})